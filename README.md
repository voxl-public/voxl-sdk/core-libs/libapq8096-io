# libvoxl_io

Userspace library for interfacing with VOXL IO ports via the Sensors DSP (sdsp). Currently supports UART, but I2C, SPI, and GPIO functions are in the works.

For more documentation on how to use, visit:
https://docs.modalai.com/voxl-io-guides/

## Build Instructions

1) Requires the voxl-hexagon (>=1.1) docker image to build.

Please follow the voxl-docker instructions:
https://gitlab.com/voxl-public/voxl-docker

2) Launch the voxl-hexagon docker image in the libvoxl-io source directory.


```bash
~/git/libvoxl_io$ voxl-docker -i voxl-hexagon
user@57f6e83bba92:~$
```

3) Install dependencies inside the docker. Specify the dependencies should be pulled from either the development (dev) or stable modalai package repos. If building the master branch you should specify `stable`, otherwise `dev`.

```bash
./install_build_deps.sh apq8096 staging
```

4) Run build.sh inside the docker.

```bash
user@57f6e83bba92:~$ ./build.sh
```

## Installation

4) Generate a deb or ipk package inside the docker.

```bash
user@57f6e83bba92:~$ ./make_package.sh ipk
```

5) You can now push the ipk package to the VOXL and install with opkg however you like. To do this over ADB, you may use the included helper script: deploy_to_voxl.sh. Do this outside of docker as your docker image probably doesn't have usb permissions for ADB.

```bash
~/git/libvoxl_io$ ./deploy_to_voxl.sh
```
